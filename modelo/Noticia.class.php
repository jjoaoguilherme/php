<?php
    class Noticia{
        private $titulo;
        private $id;
        private $texto;
        private $assunto;

        public function setTitulo($t){
            $this->titulo = ($t != NULL) ? addslashes($t) : NULL;
        }
        public function getTitulo(){
            return $this->titulo;
        }

        public function setId($id){
            $this->id = ($id != NULL) ? addslashes($id) : NULL;
        }
        public function getId(){
            return $this->id;
        }

        public function setTexto($texto){
            $this->texto = ($texto != NULL) ? addslashes($texto) : NULL;
        }
        public function getTexto(){
            return $this->texto;
        }

        public function setAssunto($as){
            $this->assunto = ($as != NULL) ? addslashes($as) : NULL;
        }
        public function getAssunto(){
            return $this->assunto;
        }
    
    } 
?>