<?php
    try{
        require_once("controle/NoticiaControle.class.php");
        $executa = new NoticiaControle();
        $noticia = new Noticia();
        $noticia->setTexto($_POST['texto']);
        $noticia->setTitulo($_POST['titulo']);
        $noticia->setAssunto($_POST['assunto']);
        if($executa->adicionarNoticia($noticia)){
            session_start();
            $_SESSION['erro'] = "deu certo";
            
        }else{
            throw new Exception("Erro ao inserir.");
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
    }
    header("location:noticias.php");
?>