<?php
require_once("conection.class.php");
require_once("modelo/Image.class.php");
final class ImageContr{
    public function adicionarImg($imagem){
        $conexao = new Conection("controle/xuxu.ini");
        $imagemTipo = explode("/", $imagem->getTipo());
        $imagemTipo = $imagemTipo[1];
        $imagemBin = file_get_contents($imagem->getImg());
        $sql="INSERT INTO imagem(titulo,img,tipo) VALUES(:tit,:i,:tp)";
        $comando = $conexao->getConection()->prepare($sql);
        $comando->bindValue("tit", $imagem->getTitulo());
        $comando->bindValue("i", $imagemBin);
        $comando->bindValue("tp", $imagemTipo);
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
        
    }
    public function Selecionar(){
        $conexao = new Conection("controle/xuxu.ini");
        $comando = $conexao->getConection()->prepare("SELECT * FROM imagem ORDER BY id DESC");
        $comando->execute();
        $resultado = $comando->fetchAll();
        $lista = [];
        foreach($resultado as $item){
            $atualizar= new Image ();
            $atualizar->setId($item->id);
            $atualizar->setTitulo($item->titulo);
            $atualizar->setTipo($item->tipo);
            array_push($lista, $atualizar);
    }
    $conexao->__destruct();
        return $lista;
    }
    public function SelecionarId($id){
        $conexao = new Conection("controle/xuxu.ini");
        $comando = $conexao->getConection()->prepare("SELECT * FROM imagem WHERE id=:id");
        $comando->bindParam("id", $id);
        $comando->execute();
        $consulta = $comando->fetch();
        $atualizar = new Image();
        $atualizar->setId($consulta->id);
        $atualizar->setImg($consulta->img);
        $atualizar->setTipo($consulta->tipo);
        $atualizar->setTitulo($consulta->titulo);
        $conexao->__destruct();
        return $atualizar;
   
    }
    public function Atualizar($atualizar){
        $conexao = new Conection("controle/xuxu.ini");
        $imagemTipo = explode("/", $atualizar->getTipo());
        $imagemTipo = $imagemTipo[1];
        $imagemBin = file_get_contents($atualizar->getImg());
        $sql= "UPDATE imagem SET titulo=:tit, img=:i, tipo=:tp WHERE id=:id";
        $comando = $conexao->getConection()->prepare($sql);
        $comando->bindValue("id", $atualizar->getId());
        $comando->bindValue("tit", $atualizar->getTitulo());
        $comando->bindValue("i", $imagemBin);
        $comando->bindValue("tp", $imagemTipo);
        $comando->execute();
        $conexao->__destruct();
        return true;
    }

    public function deletarImg($id){
        $conexao=new Conection("controle/xuxu.ini");
        $del = $conexao->getConection()->prepare("DELETE FROM imagem WHERE id=:id");
        $del->bindValue(":id",$id);
        if($del->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
}
?>