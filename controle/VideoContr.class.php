<?php
require_once("conection.class.php");
require_once("modelo/Video.class.php");
final class VideoContr{
    public function adicionarVideo($video){
        $conexao = new Conection("controle/xuxu.ini");
        $sql="INSERT INTO video(titulo,vd,tipo) VALUES(:tit,:v,:tp)";
        $comando = $conexao->getConection()->prepare($sql);
        $comando->bindValue("tit", $video->getTitulo());
        $comando->bindValue("v", $video->getVd());
        $comando->bindValue("tp", $video->getTipo());
        if($comando->execute()){
            $conexao->__destruct();
            return true;
        }else{
            $conexao->__destruct();
            return false;
        }
    }
        public function SelecionarVideo(){
            $conexao = new Conection("controle/xuxu.ini");
            $comando = $conexao->getConection()->prepare("SELECT * FROM video ORDER BY id DESC");
            $comando->execute();
            $resultado = $comando->fetchAll();
            $lista = [];
            foreach($resultado as $item){
                $atualizar= new Video();
                $atualizar->setId($item->id);
                $atualizar->setTitulo($item->titulo);
                $atualizar->setTipo($item->tipo);
                array_push($lista, $atualizar);
        }
        $conexao->__destruct();
            return $lista;
        }
        public function SelecionarId($id){
            $conexao = new Conection("controle/xuxu.ini");
            $comando = $conexao->getConection()->prepare("SELECT * FROM video WHERE id=:id");
            $comando->bindParam("id", $id);
            $comando->execute();
            $consulta = $comando->fetch();
            $atualizar = new Video();
            $atualizar->setId($consulta->id);
            $atualizar->setVd($consulta->vd);
            $atualizar->setTipo($consulta->tipo);
            $atualizar->setTitulo($consulta->titulo);
            $conexao->__destruct();
            return $atualizar;
       
        }
        public function AtualizarVi($atualizar){
            $conexao = new Conection("controle/xuxu.ini");
            $videoTipo = explode("/", $atualizar->getTipo());
            $videoTipo = $videoTipo[1];
            $videoBin = file_get_contents($atualizar->getVd());
            $sql= "UPDATE video SET titulo=:tit, img=:i, tipo=:tp WHERE id=:id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue("id", $atualizar->getId());
            $comando->bindValue("tit", $atualizar->getTitulo());
            $comando->bindValue("i", $videoBin);
            $comando->bindValue("tp", $videoTipo);
            $comando->execute();
            $conexao->__destruct();
            return true;
        }
    
        public function deletarVi($id){
            $conexao=new Conection("controle/xuxu.ini");
            $del = $conexao->getConection()->prepare("DELETE FROM video WHERE id=:id");
            $del->bindValue(":id",$id);
            if($del->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }

    }
    

?>