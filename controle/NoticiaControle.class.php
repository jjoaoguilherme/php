<?php
    require_once("conection.class.php");
    require_once("modelo/Noticia.class.php");
    final class NoticiaControle{
        public function consultaTodos(){
            $conexao = new Conection("controle/xuxu.ini");
            //COMANDO SQL PARA SELECIONAR OS DADOS
            $sql = "SELECT * FROM noticia";
            $comando = $conexao->getConection()->prepare($sql);
            //executa o comando sql
            $comando->execute();
            $resu = $comando->fetchAll();
            //faz a varredura do array
            $lista = array();
            foreach($resu as $item){
                $noticia = new Noticia();
                $noticia->setId($item->id);
                $noticia->setTitulo($item->titulo);
                $noticia->setTexto($item->texto);
                $noticia->setAssunto($item->assunto);
                array_push($lista, $noticia);
            }
            $conexao->__destruct();
            return $lista;
        }
        public function adicionarNoticia($noticia){
            //faz a conexao
            $conexao = new Conection("controle/xuxu.ini");
            //COMANDO SQL PARA INSERIR OS DADOS
            $sql = "INSERT INTO noticia (titulo, texto, assunto) VALUES (:ti,:te,:ass)";
            //prepara para ser modificada pelo php
            $comando = $conexao->getConection()->prepare($sql);
            //substitue os valores de referencia para os valores das variaveis do livro
            $comando->bindValue(":ti",$noticia->getTitulo());
            $comando->bindValue(":te",$noticia->getTexto());
            $comando->bindValue(":ass",$noticia->getAssunto());
            //executa o comando sql
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function deletarNoticia($id){
            $conexao = new Conection("controle/xuxu.ini");
            //deleta livro
            $del = $conexao->getConection()->prepare("DELETE FROM noticia WHERE id=:id");
            $del->bindValue(":id",$id);
            if($del->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function selecionarId($id){
            $conexao = new Conection("controle/xuxu.ini");
            //seleciona a noticia pelo id
            $sql = "SELECT * FROM noticia WHERE id=:id";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":id", $id);
            $comando->execute();
            $resu = $comando->fetch();
            $noticia = new Noticia();
            $noticia->setId($resu->id);
            $noticia->setTitulo($resu->titulo);
            $noticia->setTexto($resu->texto);
            $noticia->setAssunto($resu->assunto);
            $conexao->__destruct();
            return $noticia;
        }
        public function selecionarNome($nome){
            $conexao = new Conection("controle/xuxu.ini"); 
            //seleciona pelo nome         
            $sql = "SELECT * FROM noticia WHERE titulo LIKE :nome";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindValue(":nome", "%$nome%");
            $comando->execute();
            $lista = [];
            $resu = $comando->fetchAll();
            foreach($resu as $item){
                $noticia = new Noticia(); 
                $noticia->setId($item->id);
                $noticia->setTitulo($item->titulo);
                $noticia->setTexto($item->texto);
                $noticia->setAssunto($item->assunto);
                array_push($lista,$noticia);
            }
            
            return $lista;$conexao->__destruct();
        }

        public function atualizarNoticia($noticia){
            $conexao = new Conection("controle/xuxu.ini");
            $sql = "UPDATE noticia SET titulo=:tit, texto=:tex, assunto=:ass WHERE id=:id;";
            $comando = $conexao->getConection()->prepare($sql);
            $comando->bindParam("tit",$noticia->getTitulo());
            $comando->bindParam("ass",$noticia->getAssunto());
            $comando->bindParam("tex",$noticia->getTexto());
            $comando->bindParam("id",$noticia->getId());
            $comando->execute();
            $conexao->__destruct();
           
        }
    }


?>