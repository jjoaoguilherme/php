<?php
	require_once("controle/NoticiaControle.class.php");
	$comando=new NoticiaControle();
	echo"<meta charset='utf-8'/>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
		<meta name='viewport' content='width=device=width, initial-scale=1.0, maximum-scale=1.0'>
		<link rel='stylesheet' type='text/css' href='semantic/semantic.min.css'>
		<link rel='stylesheet' type='text/css' href='css/style.css'>
	";

	session_start();

	echo"
	<div class='ui fixed inverted menu'></br>
		<a class='item' href='home.php'>
				Home
			</a>
			<a class='active item' href='noticias.php'>
				Notícias
			</a>
			<a class='item' href='vi.php'>
				Vídeos e Imagens
		</a>
		
	";
	if(!isset($_SESSION['administrador'])){
		echo"
			<div class='right menu'>
			<form action='processa.php' method='post'>
					<div class='ui right aligned category search item '>
							<div class='ui transparent icon input '>
									<div class='ui input'>
											<input type='text' name='user' placeholder='Usuário'>
											<input type='password' name='pwd' placeholder='Senha'>
									</div>
									<button class='ui icon button ' type='submit'>
											Entrar
									</button>
							</div>
					</div>
			</form>
			</div>
			</div>
			";
	}else{
		echo"
			<div class='ui right inverted icon menu'>
				<a class='item'  href='modifnot.php'>
					<i class='pencil alternate icon'></i>
				</a>
				<a class='item' href='sair.php'>
					<i class='sign out alternate icon'></i>
				</a>
			</div>
		</div>
		";
	}

	echo"<br>
		<br>
		<div class='ui raised very padded text container segment' id='curi'>
			<h2 class='ui header'>Notícias</h2>
			";

	$resposta=$comando->consultaTodos();
	foreach($resposta as $consul){
		echo"
			<tr>
				<td data-label='titulo'><h3><b>{$consul->getTitulo()}</b></h3></td>
				<td data-label='texto'><h4>{$consul->getTexto()}</h4></td>
				<td data-label='assunto'><h6>{$consul->getAssunto()}</h6></td><br>
				<a href='deletar.php?id={$consul->getId()}'>Excluir</a>
				<a href='atualizarnoti.php?id={$consul->getId()}'>Editar</a>
			</tr>
			<br>
			<br>
		";
	}

	if(isset($_SESSION['administrador'])){
		echo"
			<form class='ui form' action='processnoti.php' method='post'>
				<div class='field'>
				<label>Insira o título da notícia:</label>
				<input type='text' name='titulo' placeholder='Título'>
				</div>
				<div class='field'>
				<label>Insira o conteúdo da notícia:</label>
				<input type='text' name='texto' placeholder='Conteúdo'>
				</div>
				<div class='field'>
				<label>Insira o assunto da notícia:</label>
				<input type='text' name='assunto' placeholder='Assunto'>
				</div>
				<button class='ui button' type='submit'>Publicar</button>
			</form>
		";
	}

	echo"</div>
	";

	echo "<script src='semantic/semantic.js'></script>";

?>